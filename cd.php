<?php

class Path
{
    public $current_path;

    function __construct($path)
    {
        $this->current_path = $path;
    }

    public function cd($new_path)
    {
        $dirs = explode('/', $new_path);
        $current_dir = explode('/', $this->current_path);

        if ( strstr($new_path, '/') && strpos($new_path, '/') === 0 ){
            $dirs = explode('/', $new_path);
            $current_dir = array();
        }

        foreach ($dirs as $dir) 
        {
            if ($dir === '..'){
                array_pop($current_dir);
            }else if($dir === '.'){
                // do nothing
            }else{
                array_push($current_dir, $dir);
            }
        }

        $this->current_path = implode('/', $current_dir);

        return $this;
    }
}

$path = new Path('/a/b/c/d');
$path->cd('../x');
//$path->cd('./x');
//$path->cd('x');
//$path->cd('/a');
//$path->cd('/d/e/../a');
//$path->cd('../../e/../f');
echo $path->current_path;

?>